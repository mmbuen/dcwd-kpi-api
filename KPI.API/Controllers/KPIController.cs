﻿using KPI.Common.Extensions;
using KPI.Common.RequestModels;
using KPI.Service.Abstract;
using System.Threading.Tasks;
using System.Web.Http;

namespace KPI.API.Controllers
{
    [RoutePrefix("api/kpi")]
    public class KPIController : ApiController
    {
		private readonly IKPIService _kpiService;
		public KPIController(IKPIService kpiService)
		{
			_kpiService = kpiService;
		}

		[HttpGet, Route("details")]
		public async Task<IHttpActionResult> GetKPIDetails([FromUri] KPIFilterRequest request)
		{
			var result = await _kpiService.GetKPIDetails(request);
			if (result.IsNull())
				return NotFound();
			return Ok(result);
		}

		[HttpGet, Route("detailsv2")]
		public async Task<IHttpActionResult> GetKPIDetailsv2([FromUri] KPIFilterRequest request)
		{
			var result = await _kpiService.GetKPIDetailsv2(request);
			if (result.IsNull())
				return NotFound();
			return Ok(result);
		}

		[HttpGet, Route("all")]
		public async Task<IHttpActionResult> GetKPIAll([FromUri] KPITableFilterRequest request)
		{
			var result = await _kpiService.GetKPIAll(request);
			if (result.IsNull())
				return NotFound();
			return Ok(result);
		}

		[HttpGet, Route("list")]
		public async Task<IHttpActionResult> GetKPIAllList([FromUri] KPITableFilterRequest request)
		{
			var result = await _kpiService.GetKPIAllList(request);
			if (result.IsNull())
				return NotFound();
			return Ok(result);
		}
		
	}
}