﻿using KPI.Common.Extensions;
using KPI.Common.RequestModels;
using KPI.Service.Abstract;
using System.Threading.Tasks;
using System.Web.Http;

namespace KPI.API.Controllers
{
    [RoutePrefix("api/score")]
    public class ScoreController : ApiController
    {
        private readonly IScoreService _scoreService;
        public ScoreController(IScoreService scoreService)
        {
            _scoreService = scoreService;
        }

        [HttpGet, Route("total")]
        public async Task<IHttpActionResult> GetKPIScore([FromUri] KPIScoreRequest request)
        {
            var result = await _scoreService.GetKPIScore(request);
            if (result.IsNull())
                return NotFound();
            return Ok(result);
        }
    }
}