﻿using KPI.Common.Extensions;
using KPI.Service.Abstract;
using System.Threading.Tasks;
using System.Web.Http;

namespace KPI.API.Controllers
{
    [RoutePrefix("api/department")]
    public class DepartmentController : ApiController
    {
        private readonly IDepartmentService _deptService;
        public DepartmentController(IDepartmentService deptService)
        {
            _deptService = deptService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetDepartment()
        {
            var result = await _deptService.GetDepartments();
            if (result.IsNull())
                return NotFound();
            return Ok(result);
        }
    }
}