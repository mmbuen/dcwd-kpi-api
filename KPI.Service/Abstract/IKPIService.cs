﻿using KPI.Common.RequestModels;
using KPI.Common.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KPI.Service.Abstract
{
    public interface IKPIService
    {
        Task<List<KPIResponse>> GetKPIDetails(KPIFilterRequest request);

        Task<List<KPIResponseList>> GetKPIDetailsv2(KPIFilterRequest request);
        Task<List<KPITableResponse>> GetKPIAll(KPITableFilterRequest request);

        Task<List<KPIListResponse>> GetKPIAllList(KPITableFilterRequest request);
    }
}
