﻿using KPI.Common.RequestModels;
using KPI.Common.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KPI.Service.Abstract
{
    public interface IScoreService
    {
        Task<List<KPIScoreResponse>> GetKPIScore(KPIScoreRequest request);
    }
}
