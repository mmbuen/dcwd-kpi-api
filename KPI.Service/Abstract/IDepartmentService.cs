﻿using KPI.Common.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KPI.Service.Abstract
{
    public interface IDepartmentService
    {
        Task<List<DepartmentResponse>> GetDepartments();
    }
}
