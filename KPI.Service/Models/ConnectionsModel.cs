﻿using System.Data.Odbc;
using System.Data.SqlClient;

namespace KPI.Service.Models
{
    public class ODBCConnectionModel
    {
        public OdbcConnection Connection { get; set; }
        public OdbcDataReader Reader { get; set; }
    }

    public class SQLConnectionModel
    {
        public SqlConnection Connection { get; set; }
        public SqlDataReader Reader { get; set; }
    }
}
