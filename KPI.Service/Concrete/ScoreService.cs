﻿using KPI.Common.Extensions;
using KPI.Common.RequestModels;
using KPI.Common.ResponseModels;
using KPI.Service.Abstract;
using KPI.Service.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPI.Service.Concrete
{
    public class ScoreService : IScoreService
	{
		public ScoreService()
        {

        }

		public async Task<List<KPIScoreResponse>> GetKPIScore(KPIScoreRequest request)
		{
			var yearFrom = request.DateFrom.GetYearMonth();
			var yearTo = request.DateTo.GetYearMonth();

			var list = new List<KPIScoreResponse>();
			string query = $"SELECT Period, ID as CriteriaSort, CriteriaName, sum(Points) as ScoreTotal, PassingScore, sum(convert(float,maxScore)) as MaxScore from View_KPI_Score where Period={yearFrom} GROUP BY Period, ID, CriteriaName, PassingScore";
			var config = Connections.SQLConnect(query, default);
			if (config.Reader.HasRows)
			{
				while (config.Reader.Read())
				{
					list.Add(new KPIScoreResponse
					{
						Period = config.Reader["Period"].ToString(),
						CritSort = config.Reader["CriteriaSort"].ToString(),
						CritName = config.Reader["CriteriaName"].ToString(),
						Score = config.Reader["ScoreTotal"].ToString(),
						PassingScore = config.Reader["PassingScore"].ToString(),
						MaxScore = config.Reader["MaxScore"].ToString()
					});
				}
				list = list.OrderBy(q => q.CritSort).ToList();
			}
			config.Reader.Close();
			config.Connection.Close();
			return list;
		}
	}
}
