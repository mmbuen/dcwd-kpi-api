﻿using KPI.Common.Extensions;
using KPI.Common.RequestModels;
using KPI.Common.ResponseModels;
using KPI.Service.Abstract;
using KPI.Service.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPI.Service.Concrete
{
	public class KPIService : IKPIService
	{
		public KPIService()
		{

		}

		public async Task<List<KPIResponse>> GetKPIDetails(KPIFilterRequest request)
		{
			var yearFrom = request.DateFrom.GetYearMonth();
			var yearTo = request.DateTo.GetYearMonth();


			var list = new List<KPIResponse>();
			string query = $"SELECT * FROM View_KPI_Details where Period >= '{ yearFrom }' AND Period <= '{ yearTo}' AND (Department LIKE '{request.Code}%' OR AGM_Group LIKE '{request.Code}%' or Department='All Departments') ORDER BY CriteriaSort, PISort, SISort";
			var config = Connections.SQLConnect(query, default);
			if (config.Reader.HasRows)
			{
				var prevPerfInd = "";
				var comp = new List<KPIComponent>();
				while (config.Reader.Read())
				{
					var period = config.Reader["Period"].ToString();
					var agmgroup = config.Reader["AGM_Group"].ToString();
					//var dept = config.Reader["Department"].ToString();
					var critsort = config.Reader["CriteriaSort"].ToString();
					var critname = config.Reader["CriteriaName"].ToString();
					var mfo = config.Reader["MFO"].ToString();
					var perfind = config.Reader["Performance_Indicator"].ToString();
					var figure = config.Reader["Figure"].ToString();
					var figuremainytd = config.Reader["FigureMainYTD"].ToString();
					var target = config.Reader["Target"].ToString();
					var score = config.Reader["Score"].ToString();
					var maxscore = config.Reader["MaxScore"].ToString();

					if (prevPerfInd != perfind)
					{
						comp = new List<KPIComponent>();
						list.Add(new KPIResponse
						{
							Period = period,
							AGMGroup = agmgroup,
							//Dept = dept,
							CritSort = critsort,
							CritName = critname,
							Mfo = mfo,
							PerfInd = perfind,
							Figure = figure,
							FigureMainYTD = figuremainytd,
							Target = target,
							Score = score,
							MaxScore=maxscore,
							Components = comp,
						});
					}

					comp.Add(new KPIComponent
					{
						CompName = config.Reader["ComponentName"].ToString(),
						FigureComp = config.Reader["FigureComponent"].ToString(),
						FigureYTD = config.Reader["FigureYTD"].ToString(),
						TargetComp = config.Reader["TargetComp"].ToString(),
						Dept = config.Reader["Department"].ToString(),

					});

					prevPerfInd = perfind;
				}
				list = list.OrderBy(q => q.CritSort).ToList();
			}
			config.Reader.Close();
			config.Connection.Close();
			return list;
		}

		public async Task<List<KPIResponseList>> GetKPIDetailsv2(KPIFilterRequest request)
		{
			var yearFrom = request.DateFrom.GetYearMonth();
			var yearTo = request.DateTo.GetYearMonth();


			var list = new List<KPIResponseList>();
			string query = $"SELECT * FROM View_KPI_Details_v2 where Period >= '{ yearFrom }' AND Period <= '{ yearTo}' AND (Department LIKE '{request.Code}%' OR AGM_Group LIKE '{request.Code}%' or Department='All Departments') ORDER BY CriteriaSort, PISort, SISort, CompID, CID";
			var config = Connections.SQLConnect(query, default);
			if (config.Reader.HasRows)
			{
				var prevPerfInd = ""; var prevComp = ""; var prevCompLvl2 = "";
				var comp = new List<KPIComponentList>();
				var child = new List<KPIComponentList>(); var childlvl2= new List<KPIComponentList>();
				while (config.Reader.Read())
				{
					var period = config.Reader["Period"].ToString();
					var agmgroup = config.Reader["AGM_Group"].ToString();
					//var dept = config.Reader["Department"].ToString();
					var critsort = config.Reader["CriteriaSort"].ToString();
					var critname = config.Reader["CriteriaName"].ToString();
					var mfo = config.Reader["MFO"].ToString();
					var perfind = config.Reader["Performance_Indicator"].ToString();
					var figure = config.Reader["Figure"].ToString();
					var figuremainytd = config.Reader["FigureMainYTD"].ToString();
					var target = config.Reader["Target"].ToString();
					var targetYear = config.Reader["TargetYear"].ToString();
					var score = config.Reader["Score"].ToString();
					var maxscore = config.Reader["MaxScore"].ToString();

					if (prevPerfInd != perfind)
					{
						comp = new List<KPIComponentList>();
						list.Add(new KPIResponseList
						{
							Period = period,
							AGMGroup = agmgroup,
							//Dept = dept,
							CritSort = critsort,
							CritName = critname,
							Mfo = mfo,
							PerfInd = perfind,
							Figure = figure,
							FigureMainYTD = figuremainytd,
							Target = target,
							TargetYear = targetYear,
							Score = score,
							MaxScore = maxscore,
							Components = comp,
						});
					}



					if (prevComp!=config.Reader["CompID"].ToString() || prevPerfInd != perfind)
                    {
						child = new List<KPIComponentList>();

						comp.Add(new KPIComponentList
						{
							Key = config.Reader["CompID"].ToString(),
							CompName = config.Reader["Component"].ToString(),
							FigureComp = config.Reader["FigureComponent"].ToString(),
							FigureYTD = config.Reader["FigureYTD"].ToString(),
							TargetComp = config.Reader["TargetComp"].ToString(),
							RespDiv = config.Reader["RespParty"].ToString(),
							Dept = config.Reader["Department"].ToString(),
							Children = child
						});
					}

					if (config.Reader["SubComponent"].ToString() != "")
					{
						if (prevCompLvl2 != config.Reader["SubCompID"].ToString() || prevComp != config.Reader["CompID"].ToString())
						{
							childlvl2 = new List<KPIComponentList>();
							child.Add(new KPIComponentList
							{
								Key = config.Reader["SubCompID"].ToString(),
								CompName = config.Reader["SubComponent"].ToString(),
								FigureComp = config.Reader["FigureComponent"].ToString(),
								FigureYTD = config.Reader["FigureYTD"].ToString(),
								TargetComp = config.Reader["TargetComp"].ToString(),
								RespDiv = config.Reader["RespParty"].ToString(),
								Dept = config.Reader["Department"].ToString(),
								Children = childlvl2
							});
						}
					}

					if (config.Reader["SubComponentLvl2"].ToString() != "")
					{
						childlvl2.Add(new KPIComponentList
						{
							Key = config.Reader["SubCompLvl2ID"].ToString(),
							CompName = config.Reader["SubComponentLvl2"].ToString(),
							FigureComp = config.Reader["FigureComponent"].ToString(),
							FigureYTD = config.Reader["FigureYTD"].ToString(),
							TargetComp = config.Reader["TargetComp"].ToString(),
							RespDiv = config.Reader["RespParty"].ToString(),
							Dept = config.Reader["Department"].ToString(),
						});
					}

					prevCompLvl2 = config.Reader["SubCompID"].ToString();

					prevComp = config.Reader["CompID"].ToString();

					prevPerfInd = perfind;
				}
				list = list.OrderBy(q => q.CritSort).ToList();
			}
			config.Reader.Close();
			config.Connection.Close();
			return list;
		}

		public async Task<List<KPITableResponse>> GetKPIAll(KPITableFilterRequest request)
		{
			var list = new List<KPITableResponse>();
			string query = $"SELECT * FROM View_KPI_List where (Department LIKE '{request.Code}%' OR AGMGroup LIKE '{request.Code}%') or isnull('{request.Code}', '') = '' order by id";
			var config = Connections.SQLConnect(query, default);
			if (config.Reader.HasRows)
			{
				var prevPerfInd = "";
				var comp = new List<KPITableComponent>();
				while (config.Reader.Read())
				{
					var critCode = config.Reader["CriteriaCode"].ToString();
					var critname = config.Reader["CriteriaName"].ToString();
					var perfSort = config.Reader["PISort"].ToString();
					var mfo = config.Reader["MFO"].ToString();
					var perfind = config.Reader["PerfInd"].ToString();


					if (prevPerfInd != perfind)
					{
						comp = new List<KPITableComponent>();
						list.Add(new KPITableResponse
						{
							CritCode = critCode,
							CritName = critname,
							PerfSort = perfSort,
							Mfo = mfo,
							PerfInd = perfind,
							Components = comp,
						});
					}

					comp.Add(new KPITableComponent
					{
						ID = config.Reader["ID"].ToString(),
						CompName = config.Reader["ComponentName"].ToString(),
						Dept = config.Reader["Department"].ToString(),
						AGMGroup = config.Reader["AGMGroup"].ToString(),

					});

					prevPerfInd = perfind;
				}
				list = list.OrderBy(q => q.PerfSort).OrderBy(q => q.CritCode).ToList();
			}
			config.Reader.Close();
			config.Connection.Close();
			return list;
		}

		public async Task<List<KPIListResponse>> GetKPIAllList(KPITableFilterRequest request)
		{
			var list = new List<KPIListResponse>();
			string query = $"SELECT * FROM View_KPI_List where (Department LIKE '{request.Code}%' OR AGMGroup LIKE '{request.Code}%') or isnull('{request.Code}', '') = '' order by id";
			var config = Connections.SQLConnect(query, default);
			if (config.Reader.HasRows)
			{
				while (config.Reader.Read())
				{
					var critCode = config.Reader["CriteriaCode"].ToString();
					var critname = config.Reader["CriteriaName"].ToString();
					var perfSort = config.Reader["PISort"].ToString();
					var mfo = config.Reader["MFO"].ToString();
					var perfind = config.Reader["PerfInd"].ToString();
					var compName = config.Reader["ComponentName"].ToString();
					var dept = config.Reader["Department"].ToString();
					var agmGroup = config.Reader["AGMGroup"].ToString();

					list.Add(new KPIListResponse
					{
						CritCode = critCode,
						CritName = critname,
						PerfSort = perfSort,
						Mfo = mfo,
						PerfInd = perfind,
						CompName = compName,
						Dept = dept,
						AGMGroup = agmGroup
					});
				}
				list = list.OrderBy(q => q.PerfSort).OrderBy(q => q.CritCode).ToList();
			}
			config.Reader.Close();
			config.Connection.Close();
			return list;
		}

		
	}
}
