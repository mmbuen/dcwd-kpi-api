﻿using KPI.Common.ResponseModels;
using KPI.Service.Abstract;
using KPI.Service.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPI.Service.Concrete
{
    public class DepartmentService : IDepartmentService
    {
		public DepartmentService()
        {

        }

		public async Task<List<DepartmentResponse>> GetDepartments()
        {
			var list = new List<DepartmentResponse>();
			string query = "SELECT B.DepartmentCode as AGMCode, B.Acronym as AGMGroup, A.DepartmentCode,A.Acronym, A.[Description] from Department_KPI A join Department_KPI B on A.AGGroup=B.DepartmentCode order by B.Acronym";
			var config = Connections.SQLConnect(query, default);
			if (config.Reader.HasRows)
			{
				var prevAgmGroup = "";
				var agm = new List<DepartmentData>();
				while (config.Reader.Read())
				{
					var agmcode = config.Reader["AGMCode"].ToString();
					var agmgroup = config.Reader["AGMGroup"].ToString();

					if (prevAgmGroup != agmgroup)
					{
						agm = new List<DepartmentData>();
						list.Add(new DepartmentResponse
						{
							AGMCode = agmcode,
							AGMGroup = agmgroup,
							Departments = agm,

						});
					}
					agm.Add(new DepartmentData
					{
						AGMGroupDept = agmgroup,
						DepartmentCode = config.Reader["DepartmentCode"].ToString(),
						Acronym = config.Reader["Acronym"].ToString(),
						Description = config.Reader["Description"].ToString(),
					});
					prevAgmGroup = agmgroup;
				}
				list = list.OrderBy(q => q.AGMGroup).Distinct().ToList();
			}
			config.Reader.Close();
			config.Connection.Close();
			return list;
		}
    }
}
