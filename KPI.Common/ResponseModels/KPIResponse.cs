﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace KPI.Common.ResponseModels
{
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class KPIResponse
    {
		public string Period { get; set; }
		public string AGMGroup { get; set; }
		//public string Dept { get; set; }
		public string CritSort { get; set; }
		public string CritName { get; set; }
		public string Mfo { get; set; }
		public string PerfInd { get; set; }
		public string Figure { get; set; }
		public string FigureMainYTD { get; set; }
		public string Target { get; set; }
		public string Score { get; set; }
		public string MaxScore { get; set; }
		public List<KPIComponent> Components { get; set; }
	}

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class KPIResponseList
	{
		public string Period { get; set; }
		public string AGMGroup { get; set; }
		//public string Dept { get; set; }
		public string CritSort { get; set; }
		public string CritName { get; set; }
		public string Mfo { get; set; }
		public string PerfInd { get; set; }
		public string Figure { get; set; }
		public string FigureMainYTD { get; set; }
		public string Target { get; set; }
		public string TargetYear { get; set; }
		public string Score { get; set; }
		public string MaxScore { get; set; }
		public List<KPIComponentList> Components { get; set; }
	}

	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class KPIComponent
	{
		
		public string CompName { get; set; }
		public string FigureComp { get; set; }
		public string FigureYTD { get; set; }
		public string TargetComp { get; set; }
		public string Dept { get; set; }
	}

	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class KPIComponentList
	{
		public string Key { get; set; }
		public string CompName { get; set; }
		public string FigureComp { get; set; }
		public string FigureYTD { get; set; }
		public string TargetComp { get; set; }
		public string RespDiv { get; set; }
		public string Dept { get; set; }

		public List<KPIComponentList> Children { get; set; }

		public bool ShouldSerializeChildren()
		{
			bool s=false;
			if (Children != null)
            {
				foreach (KPIComponentList c in Children)
				{
					if (c.CompName != null)
					{
						return s = true;
					}
					else
					{
						return s = false;
					}
				}
			}
			

			return s;
		}
	}
}
