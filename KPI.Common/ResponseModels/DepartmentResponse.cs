﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace KPI.Common.ResponseModels
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class DepartmentResponse
    {
        public string AGMGroup { get; set; }
        public string AGMCode { get; set; }
        public List<DepartmentData> Departments { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class DepartmentData
    {
        public string AGMGroupDept { get; set; }
        public string DepartmentCode { get; set; }
        public string Acronym { get; set; }
        public string Description { get; set; }
    }
}
