﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace KPI.Common.ResponseModels
{
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class KPIListResponse
    {


		public string CritCode { get; set; }
		public string CritName { get; set; }

		public string PerfSort { get; set; }
		public string Mfo { get; set; }
		public string PerfInd { get; set; }
		public string CompName { get; set; }
		public string Dept { get; set; }

		public string AGMGroup { get; set; }
	}
}
