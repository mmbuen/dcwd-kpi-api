﻿

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KPI.Common.ResponseModels
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class KPIScoreResponse
    {
        public string Period { get; set; }
        public string CritSort { get; set; }
        public string CritName { get; set; }
        public string Score { get; set; }
        public string PassingScore { get; set; }
        public string MaxScore { get; set; }
    }
}
