﻿using System.ComponentModel;

namespace KPI.Common.Enums
{
    public enum MonthsFull
    {
        [Description("January")]
        Jan,
        [Description("February")]
        Feb,
        [Description("March")]
        Mar,
        [Description("April")]
        Apr,
        [Description("May")]
        May,
        [Description("June")]
        Jun,
        [Description("July")]
        Jul,
        [Description("August")]
        Aug,
        [Description("September")]
        Sep,
        [Description("October")]
        Oct,
        [Description("November")]
        Nov,
        [Description("December")]
        Dec,
    }
    public enum MonthsShort
    {
        [Description("Jan")]
        Jan = 1,
        [Description("Feb")]
        Feb = 2,
        [Description("Mar")]
        Mar = 3,
        [Description("Apr")]
        Apr = 4,
        [Description("May")]
        May = 5,
        [Description("Jun")]
        Jun = 6,
        [Description("Jul")]
        Jul = 7,
        [Description("Aug")]
        Aug = 8,
        [Description("Sep")]
        Sep = 9,
        [Description("Oct")]
        Oct = 10,
        [Description("Nov")]
        Nov = 11,
        [Description("Dec")]
        Dec = 12,
    }
}
