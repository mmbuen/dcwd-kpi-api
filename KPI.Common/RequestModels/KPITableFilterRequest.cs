﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KPI.Common.RequestModels
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class KPITableFilterRequest
    {
        public string Code { get; set; }
    }
}
