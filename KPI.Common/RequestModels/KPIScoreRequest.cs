﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KPI.Common.RequestModels
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class KPIScoreRequest
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
