﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace KPI.Common.Extensions
{
    public static class ObjectExtension
    {
        public static IDictionary<string, string> ToKeyValue(this object metaToken)
        {
            try
            {
                if (metaToken == null)
                {
                    return null;
                }

                JToken token = metaToken as JToken;
                if (token == null)
                {
                    return ToKeyValue(JObject.FromObject(metaToken));
                }

                if (token.HasValues)
                {
                    var contentData = new Dictionary<string, string>();
                    foreach (var child in token.Children().ToList())
                    {
                        var childContent = child.ToKeyValue();
                        if (childContent != null)
                        {
                            contentData = contentData.Concat(childContent)
                                .ToDictionary(k => k.Key, v => v.Value);
                        }
                    }

                    return contentData;
                }

                var jValue = token as JValue;
                if (jValue?.Value == null)
                {
                    return null;
                }

                var value = jValue?.Type == JTokenType.Date ?
                    jValue?.ToString("o", CultureInfo.InvariantCulture) :
                    jValue?.ToString(CultureInfo.InvariantCulture);

                return new Dictionary<string, string> { { token.Path, value } };
            }
            catch (Exception e)
            {

            }
            return default;
        }

        public static bool IsNull<TData>(this TData compare1) where TData : new()
        {
            try
            {
                if (compare1 == null)
                    return true;
                else
                {
                    var comparer1 = JsonConvert.SerializeObject(compare1);
                    var comparer2 = JsonConvert.SerializeObject(new TData());
                    return comparer1.Equals(comparer2);
                }
            }
            catch (Exception)
            {
                return default;
            }
        }

        public static TModel DeserializeTo<TModel>(this string stringValue)
        {
            try
            {
                return JsonConvert.DeserializeObject<TModel>(stringValue);
            }
            catch (Exception)
            {
                return default;
            }
        }

        public static bool IsNotNull<TData>(this TData compare1) where TData : new()
        {
            return !compare1.IsNull();
        }

        public static bool IsDateDefault(this DateTime? date)
        {
            try
            {
                if (date is null &&
                   date.Equals(new DateTime()))
                    return true;
            }
            catch (Exception)
            {

            }
            return false;
        }

        public static bool IsNull(this string strVal, bool notNull = false)
            => notNull ? !string.IsNullOrWhiteSpace(strVal) ? true : false
                : string.IsNullOrWhiteSpace(strVal) ? true : false;

        public static bool IsNull<T>(this IEnumerable<T> sequence)
            => sequence is null ? true : !sequence?.Any() ?? true;

        public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> self)
            => self.Select((item, index) => (item, index));

        public static string ToStringValue(this Enum value)
        {
            if (value == null)
                return null;

            string description = value.ToString();
            FieldInfo fieldInfo = value.GetType().GetField(description);
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                description = attributes[0].Description;

            return description;
        }

        public static IEnumerable<string> ExtractCSValue(this string csv, char seperator = ',')
            => csv.Split(seperator).AsEnumerable();

        public static string RevertToCSValue(this IEnumerable<string> csvList, char seperator = ',')
        {
            var strList = csvList.ToArray();
            var strBuilder = new StringBuilder();

            if (strList.Length == 0)
                return null;

            for (var i = 0; i < strList.Length; i++)
            {
                strBuilder.Append(i == strList.Length ?
                    strList[i] :
                    $"{strList[i]}{seperator}");
            }

            return strBuilder.ToString();
        }
    }
}
