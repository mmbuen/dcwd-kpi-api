﻿using System;
using KPI.Common.Enums;

namespace KPI.Common.Extensions
{
    public static class DateTimeNowHelper
    {
        public static DateTime GetNow()
            => DateTime.Now.Kind == DateTimeKind.Utc ? DateTime.Now : DateTime.UtcNow;
        public static DateTime ToDateTimeUtc(this long unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToUniversalTime();
            return dtDateTime;
        }
        public static bool IsValidDateFromUnix(this DateTime date) =>
            date == new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc) ||
            date == new DateTime() ?
            false : true;
        public static int GetRemainingTime(DateTime timestamp, DateTime expiryDate)
        {
            var timediff = expiryDate.Subtract(timestamp);
            return int.Parse((timediff.TotalHours * 60 * 60).ToString());
        }
        public static double GenerateRemainingTime(DateTime timestamp, DateTime expiryDate)
        {
            var timediff = expiryDate.Subtract(timestamp);
            return timediff.TotalHours * 60 * 60;
        }
        public static long GetUnixTimestampFromDate(DateTime dateTime)
        {
            var unix = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            return (long)Math.Round(unix, 0);
        }
        public static long GetUnixTimestampUnix(this DateTime dateTime)
        {
            var unix = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            return (long)Math.Round(unix, 0);
        }
        public static DateTime GetDateTimeFromUnixTime(long unixtime)
        {
            var dt = DateTimeNowHelper.GetNow();
            DateTime dtDateTime = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixtime);
            return dtDateTime;
        }
        public static (DateTime startDate, DateTime endDate) GetThisMonthDates(this DateTime dateTime)
            => (GetFirstDay(dateTime), GetLastDay(dateTime));
        private static DateTime GetFirstDay(DateTime dateNow)
        {
            var lastDayOfMonth = GetLastDay(dateNow);
            var firstDayOfMonth = new DateTime(lastDayOfMonth.Year, lastDayOfMonth.Month, 1);
            return firstDayOfMonth;
        }
        private static DateTime GetLastDay(DateTime dateNow)
            => new DateTime(dateNow.Year, dateNow.Month, 1).AddMonths(1).AddDays(-1);
        public static string GetYearMonth(this DateTime dateTime)
            => dateTime.ToString("yyyyMM");
        public static int GetDayOfYear(string year)
            => new DateTime(Convert.ToInt32(year), 12, 31).DayOfYear;
        public static int DaysInMonth(string year, string month)
            => DateTime.DaysInMonth(
                    Convert.ToInt32(year),
                    Convert.ToInt32((MonthsShort)Enum.Parse(typeof(MonthsShort), month))
               );
    }
}
